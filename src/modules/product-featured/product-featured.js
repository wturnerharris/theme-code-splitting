import Vue from 'vue'
import state from 'lib/appState'
import 'modules/image/image.vue.js'
import {variantFeaturedImage} from 'lib/util'

export default el => new Vue({
  el,
  data: {
    product: window.BARREL.product,
    state: state
  },
  computed: {
    featuredImage () {
      return variantFeaturedImage(this.state.activeVariant) || this.product.featured_image
    },
    title () {
      return this.product.title
    }
  }
})
