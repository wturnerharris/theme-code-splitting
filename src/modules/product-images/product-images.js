import Vue from 'vue'
import state from 'lib/appState'
import {handleize, fileName, variantFeaturedImage} from 'lib/util'
import 'modules/image/image.vue.js'

export default el => new Vue({
  el,
  data: {
    key: 'color',
    activeIndex: 0,
    state: state,
    product: window.BARREL.product
  },
  watch: {
    'state.activeVariant' () {
      this.activeIndex = 0
    }
  },
  computed: {
    imageHandle () {
      const defaultHandle = 'color'

      for (let i = 0; i < this.product.options.length; i++) {
        const {options = []} = this.product
        const variant = this.state.activeVariant

        if (handleize(options[i]) === this.key) {
          const value = variant[`option${i + 1}`]

          if (!value) {
            return false
          }
          return `${this.key}-${handleize(value)}`.toLowerCase()
        }
      }
      return defaultHandle
    },
    images () {
      const {images = []} = this.product
      const filtered = images
        .filter(src => ~src.indexOf(this.imageHandle))
        .filter(src => fileName(src) !== fileName(this.featuredImage))
      return filtered.length ? filtered : []
    },
    featuredImage () {
      return variantFeaturedImage(this.state.activeVariant) || this.product.featured_image
    },
    modelImages () {
      return this.product.images.reduce((images, image) => {
        if (fileName(image) === fileName(this.featuredImage)) {
          return images
        }

        if (~this.images.indexOf(fileName(image))) {
          return images
        }

        if (~image.indexOf(this.key)) {
          return images
        }

        images.push(image)

        return images
      }, [])
    }
  }
})
