export const mutations = {
  bootstrap (state) {
    state.isBootstrapped = true
  },
  setLoading (state, isLoading) {
    state.loading = isLoading
  },
  refreshCart (state, data) {
    state.loading = false

    if (data.errors) {
      state.app.cart.errors = data.errors
    } else {
      state.app.cart = data
    }
  },
  toggleMiniCart (state) {
    state.app.isMiniCartOpen = !state.app.isMiniCartOpen
  }
}
