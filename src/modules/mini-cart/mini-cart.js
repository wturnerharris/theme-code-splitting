import Vue from 'vue'
import store from 'modules/cart-grid/@store'
import 'modules/mini-cart/mini-cart.vue'

export default el => new Vue({
  el,
  store,
  created () {
    window.BARREL.bus.$on('add-to-cart', this.onAddToCart)
  },
  methods: {
    onAddToCart () {
      this.$store.dispatch('getCart')
        .then(() => {
          this.$store.commit('toggleMiniCart')
        })
    }
  }
})
