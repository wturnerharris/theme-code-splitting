import select from 'select-dom'
import on from 'dom-event'
import {set} from 'lib/util'

const elements = select.all('.js-video')

const getRatio = (el) => {
  return el.offsetHeight / el.offsetWidth
}

const updateVideo = (video, container) => {
  if (getRatio(video) > getRatio(container)) {
    video.style.height = 'auto'
    video.style.width = '100%'
  } else {
    video.style.height = '100%'
    video.style.width = 'auto'
  }
}

export default () => {
  for (let el of elements) {
    if (!el.classList.contains('is-init')) {
      let video = select('video', el) || false
      if (video) {
        let src = video.getAttribute('data-src')
        video.setAttribute('src', src)
        video.onloadedmetadata = () => {
          updateVideo(video, el)
          on(window, 'resize', () => {
            updateVideo(video, el)
          })
        }
        set(video, 'is-loaded')
      }
      el.classList.add('is-init')
    }
  }
}
