import Vue from 'vue'
import { formatPrice } from 'lib/util'
import debounce from 'lodash/debounce'

Vue.component('cart-item', {
  props: {
    image: String,
    title: String,
    price: Number,
    quantity: Number,
    index: Number,
    type: String,
    variantId: Number,
    variantTitle: String
  },
  methods: {
    updateQuantity: debounce(function (quantity) {
      this.$store.dispatch('updateItemQuantity', {
        quantity,
        id: this.variantId
      })
    }, 450),
    remove () {
      this.$store.dispatch('updateItemQuantity', {
        quantity: 0,
        id: this.variantId
      })
    }
  },
  computed: {
    formattedPrice () {
      return formatPrice(this.price)
    },
    isGiftCard () {
      return this.type.toLowerCase() === 'gift card'
    },
    totalFormattedPrice () {
      return formatPrice(this.price * this.quantity)
    }
  }
})
