import app from 'lib/init'
import bus from 'lib/bus'
__webpack_public_path__ = BRRL_PATH(BRRL_PUBLIC_PATH) // eslint-disable-line

BARREL.bus = bus

document.addEventListener('DOMContentLoaded', () => {
  app.init()
})
